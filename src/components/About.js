import React, {Component} from 'react';
import {
  View
} from 'react-native';
import {Container, Content, Text} from 'native-base';
import {H2} from 'nachos-ui';

class About extends Component {
  static displayName = 'About';

  static navigationOptions =
  ({ navigation }) => ({
    tabBarKey: navigation.state,
    tabBarLabel: 'à Propos',
    tabBarIcon: () => (
       <Icon name='plus-one' size={24} color="red" />
    )
  });

  render() {
    return(
      <Container style={{flex: 1, alignItems: 'center', justifyContent: 'center', flexDirection: 'row', paddingLeft: 0, marginTop: 0, backgroundColor: '#3d7dff'}}>
        <Content keyboardShouldPersistTaps='always' keyboardDismissMode="on-drag">

          <H2 style={{alignSelf: 'center', marginBottom: 10, color: '#FFF'}}>IDMOL</H2>
          <Text style={{alignSelf: 'center', textAlign: 'center', paddingLeft: 50, paddingRight: 50, color: '#FFF'}}>Application de profilage chimique</Text>
          <Text style={{alignSelf: 'center', textAlign: 'center', paddingLeft: 50, paddingRight: 50, color: '#FFF'}}>Production Pharma</Text>
          <Text style={{alignSelf: 'center', textAlign: 'center', paddingLeft: 50, paddingRight: 50, color: '#FFF'}}>ORALOGSOFT © 2016-2017</Text>
        </Content>
      </Container>
    )
  }

}

export default About
