import {Map, fromJS} from 'immutable';

// Initial state
const initialState = fromJS({
  users: [],
  error: ''
});

// Actions
const ADD_USER = 'SignupState/ADD_USER';
const DELETE_USER = 'SignupState/DELETE_USER';
// const USER_EXISTS = 'SignupState/USER_EXISTS';

// Action creators
// export function addUser(username, email, password) {
//   return {type: ADD_USER, username, email, password};
// }

export function addUser(username, email, password) {
  return (dispatch, getState) => {
    const users = getState().getIn(['signup', 'users']);
    // let index = users.findIndex(user => user.get('username') === username);
    let index = -1;
    try{
      index = users.findIndex(user => user.get('username') === username);
    }catch(e){
      // Not found
    }
    if(index === -1) {
      dispatch({type: ADD_USER, username, email, password});
    } else {
      return true;
    }
  }
}

// Reducer
export default function LoginStateReducer(state = initialState, action = {}) {
  switch (action.type) {
    case ADD_USER:
      return state.update('users', arr => arr.push({username: action.username, email: action.email, password: action.password}));

    case DELETE_USER:
      return state;

    // case USER_EXISTS:
    //   return state.set('error': 'User exists');
    default:
      return state;
  }
}
