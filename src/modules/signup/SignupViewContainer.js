import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import SignupView from './SignupView';
import {NavigationActions} from 'react-navigation';
import * as SignupStateActions from '../signup/SignupState';
import * as LoginStateActions from '../login/LoginState';

export default connect(
  state => ({
    users: state.getIn(['signup', 'users']),
  }),
  dispatch => {
    return {
      navigate: bindActionCreators(NavigationActions.navigate, dispatch),
      signupStateActions: bindActionCreators(SignupStateActions, dispatch),
      loginStateActions: bindActionCreators(LoginStateActions, dispatch)
    };
  }
)(SignupView);
