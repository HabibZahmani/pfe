import React, {PropTypes, Component} from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Image,
  Dimensions,
  View
} from 'react-native';
import {Container, Content, Form, Item, Input, ListItem, Text, Left, Right, Body, Button} from 'native-base';
import {H6, H3, Checkbox} from 'nachos-ui';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { NavigationActions } from 'react-navigation'

class SignupView extends Component {
  static displayName = 'SignupView';

  static navigationOptions = {
    title: 'SignUp'
  }

  state = {
    username: 'a',
    password: 'a',
    email: 'a',
    error: '',
    screenHeight: Dimensions.get('window').height,
    screenWidth: Dimensions.get('window').width
  }

  resetTo(route) {
    const actionToDispatch = NavigationActions.reset({
      index: 0,
      key: null,
      actions: [NavigationActions.navigate({ routeName: route })],
    });
    this.props.navigate(actionToDispatch);
  }

  onLayout = () => {
    this.setState({
      screenWidth: Dimensions.get('window').width,
      screenHeight: Dimensions.get('window').height,
    });
  }

  handlePress = () => {
    // alert('test');
    // this.props.signupStateActions.addUser(this.state.username, this.state.password);
    if(!this.state.username || !this.state.password || !this.state.email) {
      this.setState({error: 'L\'un des champs est vide !'});
    } else {
      this.setState({error: ''});
      var userExists = this.props.signupStateActions.addUser(this.state.username, this.state.email, this.state.password);
      if(userExists) {
        this.setState({error: 'L\'utilisateur existe !', username: '', email: '', password: ''});
      } else {
        this.props.loginStateActions.logIn(this.state.username, false);
        this.resetTo('Home');
      }
    }
  }

  render() {

    return (
      <View onLayout={(e) => this.onLayout()} style={{backgroundColor: '#F5FCFF', height: this.state.screenHeight}}>
        <Container style={{flex: 1, alignItems: 'center', justifyContent: 'center', flexDirection: 'row', margin: 50, paddingLeft: 0, marginTop: 0}}>
          <Content keyboardShouldPersistTaps='always' keyboardDismissMode="on-drag">



            <H3 style={{paddingBottom: 5}}>Signup</H3>
            <Form>
              <Item style={{marginLeft: 0}} fixedLabel>
                <Icon name='person' color="#4463e8" size={22} style={{marginRight: 10}} />
                <Input
                  ref= {(el) => { this.username = el; }}
                  onChangeText={(username) => this.setState({username})}
                  value={this.state.username}
                />
              </Item>
              <Item style={{marginLeft: 0}} fixedLabel>
                <Icon name='email' color="#4463e8" size={22} style={{marginRight: 10}}/>
                <Input
                  ref= {(el) => { this.email = el; }}
                  onChangeText={(email) => this.setState({email})}
                  value={this.state.email}
                />
              </Item>
              <Item style={{marginLeft: 0}} fixedLabel>
                <Icon name='lock' color="#4463e8" size={22} style={{marginRight: 10}}/>
                <Input secureTextEntry
                  ref= {(el) => { this.password = el; }}
                  onChangeText={(password) => this.setState({password})}
                  value={this.state.password}
                />
              </Item>

              <Button info onPress={() => this.handlePress()} style={{marginTop: 10}} block>
                <Text>S'inscrire</Text>
              </Button>
              <H6 style={{color: 'rgb(255, 156, 0)'}}>{this.state.error}</H6>



            </Form>





          </Content>
        </Container>
      </View>
    );
  }
}

export default SignupView;
