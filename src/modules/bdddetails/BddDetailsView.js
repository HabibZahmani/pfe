import React, {PropTypes, Component} from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View
} from 'react-native';
import { Container, Content, List, ListItem, Thumbnail, Text, Body, Left, Right, Button } from 'native-base';
import {H3} from 'nachos-ui';

import Icon from 'react-native-vector-icons/MaterialIcons';

class CounterView extends Component {
  static displayName = 'BddDetails';

  static navigationOptions =
  ({ navigation }) => ({
    tabBarKey: navigation.state,
    tabBarLabel: 'Détails',
    tabBarIcon: () => (
       <Icon name='plus-one' size={24} color="red" />
    ),
    // visible: !navigation.state.params ? true : navigation.state.params.visible,
  });

  _renderInfo() {
    const patient = this.props.patients.find((it) => it.get('id') === this.props.selectedPatient);
    return(
      <List>
      <ListItem itemDivider>
        <H3>{patient.get('name')}</H3>
      </ListItem>

      <ListItem key={patient.get('id')} avatar>
          <Left>
              <Icon name='healing' size={25} color='#629e62' />
          </Left>
          <Body>
              <Text>{patient.get('sexe')}</Text>
              <Text note>{patient.get('age')}</Text>
          </Body>
      </ListItem>

      <ListItem divider>
        <H3>Signature Chimique</H3>
      </ListItem>

      {patient.get('signatureChimique').entrySeq().map((it, index) => {return (
        <ListItem key={index} avatar>
            <Left>
                <Icon name='description' size={25} color='#629e62' />
            </Left>
            <Body>
                <Text>{it[0]}</Text>
                <Text note>{it[1]}</Text>
            </Body>
        </ListItem>
      )})}

    </List>
    )
  }

  render() {
    return (
      <Container>
        <Content>
          {!this.props.selectedPatient ? <Text>Nada Signor!</Text> : this._renderInfo()}
        </Content>
      </Container>
    );
  }
}

const circle = {
  borderWidth: 0,
  borderRadius: 40,
  width: 80,
  height: 80
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white'
  },
  userContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  userProfilePhoto: {
    ...circle,
    alignSelf: 'center'
  },
  counterButton: {
    ...circle,
    backgroundColor: '#349d4a',
    alignItems: 'center',
    justifyContent: 'center',
    margin: 20
  },
  counter: {
    color: 'white',
    fontSize: 20,
    textAlign: 'center'
  },
  welcome: {
    textAlign: 'center',
    color: 'black',
    marginBottom: 5,
    padding: 5
  },
  linkButton: {
    textAlign: 'center',
    color: '#CCCCCC',
    marginBottom: 10,
    padding: 5
  }
});

export default CounterView;
