import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import BddDetailsView from './BddDetailsView';
import {NavigationActions} from 'react-navigation';
import * as BddDetailsStateActions from '../bdddetails/BddDetailsState';
import * as ResultsStateActions from '../results/ResultsState';

export default connect(
  state => ({
    patients: state.getIn(['results', 'patients']),
    selectedPatient: state.getIn(['results', 'selectedPatient'])
  }),
  dispatch => {
    return {
      navigate: bindActionCreators(NavigationActions.navigate, dispatch),
      bdddetailsStateActions: bindActionCreators(BddDetailsStateActions, dispatch),
      resultsStateActions: bindActionCreators(ResultsStateActions, dispatch)
    };
  }
)(BddDetailsView);
