import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import HomeView from './HomeView';
import {NavigationActions} from 'react-navigation';
import * as HomeStateActions from '../home/HomeState';
import * as LoginStateActions from '../login/LoginState';

export default connect(
  state => ({
    username: state.getIn(['login', 'username']),
    patients: state.getIn(['results', 'patients']),
    selectedUser: state.getIn(['home', 'selectedUser'])
  }),
  dispatch => {
    return {
      navigate: bindActionCreators(NavigationActions.navigate, dispatch),
      homeStateActions: bindActionCreators(HomeStateActions, dispatch),
      loginStateActions: bindActionCreators(LoginStateActions, dispatch)
    };
  }
)(HomeView);
