import React, {PropTypes, Component} from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Image,
  View
} from 'react-native';
import { CircleButton, RoundButton, RectangleButton } from 'react-native-button-component';
import {H3} from 'nachos-ui';
import { Button, Text } from 'native-base';
import Icon from 'react-native-vector-icons/MaterialIcons';
import ActionButton from 'react-native-action-button';
import { NavigationActions } from 'react-navigation'
import Spinner from 'react-native-spinkit';
// import Icon from 'react-native-vector-icons/Ionicons';

class HomeView extends Component {
  static displayName = 'HomeView';

  static navigationOptions =
  ({ navigation }) => ({
    tabBarKey: navigation.state,
    tabBarLabel: 'Home',
    tabBarIcon: () => (
       <Icon name='plus-one' size={24} color="red" />
    ),
    // visible: !navigation.state.params ? true : navigation.state.params.visible,
  });

  state = {
    loading: false,
    download: 'download',
    downloadProgress: 0
  }

  download = () => {
    // alert(getRandomInt(0, this.props.patients.size - 1));
    const self = this;
    this.setState({ download: 'downloading', downloadProgress: 0 });
    const intervalId = setInterval(() => {
      if (this.state.downloadProgress < 100) {
        const downloadProgress = this.state.downloadProgress + 1;
        this.setState({ downloadProgress });
      } else {
        this.props.homeStateActions.selectUser(getRandomInt(1, this.props.patients.size));
        clearInterval(intervalId);
        this.setState({ download: 'download' });
        self.props.navigate({routeName: 'Results'});
      }
    }, 50);
  }

  downloaded = () => {
    this.setState({ download: 'download' });
  }

  componentDidMount() {

  }

  handlePress = () => {
    this.setState({loading: true});
    const self = this;
    setTimeout(() => {
      self.props.homeStateActions.generateAllDiseases(3);
      self.props.navigate({routeName: 'Results'});
      this.setState({loading: false});
    }, 50);
  }

  resetTo(route) {
    const actionToDispatch = NavigationActions.reset({
      index: 0,
      key: null,
      actions: [NavigationActions.navigate({ routeName: route })],
    });
    this.props.navigate(actionToDispatch);
  }

  render() {
//      types: ['CircleFlip', 'Bounce', 'Wave', 'WanderingCubes', 'Pulse', 'ChasingDots', 'ThreeBounce', 'Circle', '9CubeGrid',
// 'WordPress', 'FadingCircle', 'FadingCircleAlt', 'Arc', 'ArcAlt'],
    return (
      <View style={styles.container}>
          <H3 style={{marginBottom: 30, color: '#FFF'}}>Analyse de la trace chimique</H3>
          <CircleButton
            shape="circle"
            states={{
              download: {
                text: 'Analyse',
                backgroundColors: ['#4DC7A4', '#66D37A'],
                onPress: this.download,
              },
              downloading: {
                backgroundColors: ['#6A6AD5', '#6F86D9'],
                text: 'En cours',
                progressText: `${this.state.downloadProgress}%`,
                progress: true,
                progressFill: this.state.downloadProgress,
                onPress: this.downloaded,
              },
            }}
            buttonState={this.state.download}
          />


        <ActionButton buttonColor="rgba(231,76,60,1)">
          <ActionButton.Item buttonColor='#1abc9c' title="à propos" onPress={() => {
            // this.props.loginStateActions.logOut();
            // this.resetTo('Auth');
            this.props.navigate({routeName: 'About'});
          }}>
            <Icon name="done-all" style={styles.actionButtonIcon} />
          </ActionButton.Item>
        </ActionButton>


      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    padding: 15,
    backgroundColor: '#5ca7e0'
  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
});

export default HomeView;

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}
