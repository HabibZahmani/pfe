import {fromJS} from 'immutable';
import {loop, Effects} from 'redux-loop-symbol-ponyfill';
import {randomDiseases} from '../../utils/api';

// Initial state
const initialState = fromJS({
  selectedUser: null
});

// Actions
const GENERATE_ALL_DISEASES = 'HomeState/GENERATE_ALL_DISEASES';
const GENERATE_DISEASES = 'HomeState/GENERATE_DISEASES';
const SELECT_USER = 'HomeState/SELECT_USER';

export function selectUser(index) {
  return {type: SELECT_USER, index};
}

// Action creators
export function generateAllDiseases(number) {
  return (dispatch, getState) => {
    const diseases = randomDiseases(number);
    const username = getState().getIn(['login', 'username']);

    // Check if user exists
    let index = getState().getIn(['home', 'users']).findIndex(function(elt){return elt.get('username') === username});

    // L'utilisateur n'existe pas
    if(index === -1) {
      dispatch({type: GENERATE_ALL_DISEASES, diseases, username});
    } else {
      console.log("L'utilisateur existe");
      dispatch({type: GENERATE_DISEASES, diseases, username, index});
    }






    // const index = getState().getIn(['home', 'users']).findIndex(function(elt){return elt.get('username') === username});
    // console.log('index', index);

    // var result = getState().getIn(['home', 'users']).find(function(obj){return obj.get('username') === username});
    // if(result && result.getIn(['diseases', 'diseasesChronic'])) {
    //   alert('chronic');
    // } else {
    //   alert('Not');
    // }
}
}

// Reducer
export default function HomeStateReducer(state = initialState, action = {}) {
  switch (action.type) {
    case GENERATE_ALL_DISEASES:
      // return state.set('diseases', action.diseases.diseases).set('diseasesChronic', action.diseases.diseasesChronic);
      // return state.update('users', user => user.concat(fromJS({username: action.username, diseases: action.diseases, diseasesChronic: action.diseasesChronic}));
      return state.update('users', user => user.concat(fromJS([{username: action.username, diseases: action.diseases, diseasesChronic: action.diseasesChronic}])));
    case GENERATE_DISEASES:
      // return state.set('diseases', action.diseases.diseases);
      // let index = state.get('users').findIndex(function(elt){return elt.get('username') === username});
      return state.setIn(['users', action.index, 'diseases', 'diseases'], action.diseases.diseases);

    case SELECT_USER:
      return state.set('selectedUser', action.index);

    default:
      return state;
  }
}
