import React, {PropTypes, Component} from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Image,
  Dimensions,
  View
} from 'react-native';
import {Container, Content, Form, Item, Input, ListItem, Text, Left, Right, Body, Button} from 'native-base';
import {H6, H3, Checkbox} from 'nachos-ui';
import Icon from 'react-native-vector-icons/MaterialIcons';
import _ from 'lodash';
import { NavigationActions } from 'react-navigation'

class LoginView extends Component {
  static displayName = 'LoginView';

  static navigationOptions = {
    title: 'Login',
    header: null,
  }

  constructor(props) {
  super(props);
    this.state = {
      username: '',
      password: '',
      rememberMe: false,
      error: '',
      screenHeight: Dimensions.get('window').height,
      screenWidth: Dimensions.get('window').width
    }
  }

  onLayout = () => {
    this.setState({
      screenWidth: Dimensions.get('window').width,
      screenHeight: Dimensions.get('window').height,
    });
  }

  resetTo(route) {
    const actionToDispatch = NavigationActions.reset({
      index: 0,
      key: null,
      actions: [NavigationActions.navigate({ routeName: route })],
    });
    this.props.navigate(actionToDispatch);
  }

  handlePress = () => {
    // this.props.navigate({routeName: 'Home'});
    if(!this.state.username || !this.state.password) {
      this.setState({error: 'L\'un des champs est vide'})
    } else {
      // Les champs sont remplis
      // On vérifie si l'utilisateur existe
      const users = this.props.users.toJS();
      if(_.some(users, {username: this.state.username, password: this.state.password})) {
        this.props.loginStateActions.logIn(this.state.username, this.state.rememberMe)
        this.resetTo('Home');

      } else {
        this.setState({error: 'L\'utilisateur n\'existe pas !'})
      }
    }
  }

  render() {

    return (
      <View onLayout={(e) => this.onLayout()} style={{backgroundColor: '#F5FCFF', height: this.state.screenHeight}}>
        <Container style={{flex: 1, alignItems: 'center', justifyContent: 'center', flexDirection: 'row', margin: 50, paddingLeft: 0, marginTop: 0}}>
          <Content keyboardShouldPersistTaps='always' keyboardDismissMode="on-drag">
            <H3 style={{paddingBottom: 5}}>Login</H3>
            <Form>
              <Item style={{marginLeft: 0}} fixedLabel>
                <Icon name='person' color="#4463e8" size={22} style={{marginRight: 10}} />
                <Input
                  ref= {(el) => { this.username = el; }}
                  onChangeText={(username) => this.setState({username})}
                  value={this.state.username}
                />
              </Item>
              <Item style={{marginLeft: 0}} fixedLabel>
                <Icon name='lock' color="#4463e8" size={22} style={{marginRight: 10}}/>
                <Input secureTextEntry
                  ref= {(el) => { this.password = el; }}
                  onChangeText={(password) => this.setState({password})}
                  value={this.state.password}
                />
              </Item>

              <ListItem style={{borderBottomWidth: 0, marginLeft: 0}}>
                <Checkbox checked={this.state.rememberMe} kind='circle' onValueChange={() => this.setState({rememberMe: !this.state.rememberMe})} />
                <Body>
                  <Text>Rester connecté</Text>
                </Body>
              </ListItem>
              <Button primary onPress={() => this.handlePress()} style={{marginTop: 10}} block>
                <Text>Connexion</Text>
              </Button>
              <Button success onPress={() => this.props.navigate({routeName: 'Signup'})} style={{marginTop: 10}} block>
                <Text>S'inscrire</Text>
              </Button>
              <H6 style={{color: 'rgb(255, 156, 0)'}}>{this.state.error}</H6>



            </Form>
          </Content>
        </Container>
      </View>
    );
  }
}

export default LoginView;
