import {Map} from 'immutable';

// Initial state
const initialState = Map({
  loggedIn: false,
  username: null,
  rememberMe: false
});

// Actions
const LOG_IN = 'LoginState/LOG_IN';
const LOG_OUT = 'LoginState/LOG_OUT';

// Action creators
export function logIn(username, rememberMe) {
  return {type: LOG_IN, username, rememberMe};
}

export function logOut() {
  return {type: LOG_OUT};
}

// Reducer
export default function LoginStateReducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOG_IN:
      return state.set('loggedIn', true).set('username', action.username).set('rememberMe', action.rememberMe);

    case LOG_OUT:
      return state.set('loggedIn', false).set('username', null);
    default:
      return state;
  }
}
