import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import LoginView from './LoginView';
import {NavigationActions} from 'react-navigation';
import * as LoginStateActions from '../login/LoginState';

export default connect(
  state => ({
    loggedIn: state.getIn(['login', 'loggedIn']),
    rememberMe: state.getIn(['login', 'rememberMe']),
    username: state.getIn(['login', 'username']),
    users: state.getIn(['signup', 'users'])
  }),
  dispatch => {
    return {
      navigate: bindActionCreators(NavigationActions.navigate, dispatch),
      loginStateActions: bindActionCreators(LoginStateActions, dispatch)
    };
  }
)(LoginView);
