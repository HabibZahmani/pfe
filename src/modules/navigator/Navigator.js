import {Platform} from 'react-native';
import {TabNavigator, StackNavigator} from 'react-navigation';

// import CounterViewContainer from '../counter/CounterViewContainer';
import ResultsViewContainer from '../results/ResultsViewContainer';
import BddViewContainer from '../bdd/BddViewContainer';
import HomeViewContainer from '../home/HomeViewContainer';
import ColorViewContainer from '../colors/ColorViewContainer';
import LoginViewContainer from '../login/LoginViewContainer';
import SignupViewContainer from '../signup/SignupViewContainer';
import BddDetailsViewContainer from '../bdddetails/BddDetailsViewContainer';
import About from '../../components/About';

const headerColor = '#ff4f4f';
const activeColor = 'white';

// TabNavigator is nested inside StackNavigator
export const MainScreenNavigator = TabNavigator({
  Home: {screen: HomeViewContainer},
  Results: {screen: ResultsViewContainer},
  BddViewContainer: {screen: BddViewContainer}
}, {
  tabBarOptions: {
    ...Platform.select({
      android: {
        activeTintColor: activeColor,
        indicatorStyle: {backgroundColor: activeColor},
        style: {backgroundColor: headerColor}
      }
    })
  }
});

MainScreenNavigator.navigationOptions = {
  title: 'IDMOL',
  headerTintColor: 'white',
  headerStyle: {
    backgroundColor: headerColor
  }
};

const AuthStack = StackNavigator({
  Login: { screen: LoginViewContainer },
  Signup: { screen: SignupViewContainer },
});

AuthStack.navigationOptions = {
  title: 'Login',
  header: null
};



// Root navigator is a StackNavigator
const AppNavigator = StackNavigator({
  // Auth: {screen: AuthStack},
  Home: {screen: MainScreenNavigator},
  BddDetails: {screen: BddDetailsViewContainer},
  About: {screen: About},
  InfiniteColorStack: {screen: ColorViewContainer}
});

export default AppNavigator;
