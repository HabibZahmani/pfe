import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import BddView from './BddView';
import {NavigationActions} from 'react-navigation';
import * as BddStateActions from '../bdd/BddState';
import * as ResultsStateActions from '../results/ResultsState';

export default connect(
  state => ({
    patients: state.getIn(['results', 'patients']),
    username: state.getIn(['login', 'username']),
    selectedUser: state.getIn(['home', 'selectedUser'])
  }),
  dispatch => {
    return {
      navigate: bindActionCreators(NavigationActions.navigate, dispatch),
      bddStateActions: bindActionCreators(BddStateActions, dispatch),
      resultsStateActions: bindActionCreators(ResultsStateActions, dispatch)
    };
  }
)(BddView);
