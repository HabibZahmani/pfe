import React, {PropTypes, Component} from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Image,
  View
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';
import { Container, Content, List, ListItem, Thumbnail, Text, Body, Left, Right, Button } from 'native-base';
import {H3} from 'nachos-ui';

class BddView extends Component {
  static displayName = 'BddView';

  static navigationOptions =
  ({ navigation }) => ({
    tabBarKey: navigation.state,
    tabBarLabel: 'Base de données',
    tabBarIcon: () => (
       <Icon name='plus-one' size={24} color="red" />
    )
  });

  _renderBDDItems(patients) {
    return (
      <List>
        <ListItem itemDivider>
          <H3>BDD</H3>
        </ListItem>

        {patients.map((patient) => {
          return(
            <ListItem onPress={() => {
              this.props.resultsStateActions.selectPatient(patient.get('id'));
              this.props.navigate({routeName: 'BddDetails'});
            }} key={patient.get('id')} avatar>
                <Left>
                    <Icon name='healing' size={25} color='#629e62' />
                </Left>
                <Body>
                    <Text>{patient.get('name')}</Text>
                    <Text note>{patient.get('sexe')}</Text>
                </Body>
                <Right>
                    <Text note>{patient.get('age')}</Text>
                </Right>
            </ListItem>
          )})}
        </List>
        )
  }

  render() {
    const {patients} = this.props;
    return (
      <Container>
        <Content>
        {this._renderBDDItems(patients)}
      </Content>
    </Container>
    );
  }
}

const circle = {
  borderWidth: 0,
  borderRadius: 40,
  width: 80,
  height: 80
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white'
  }
});

export default BddView;
