import {fromJS} from 'immutable';
import {loop, Effects} from 'redux-loop-symbol-ponyfill';
import {generateRandomNumber} from '../../services/randomNumberService';

// Initial state
// M for male // F for female
// A for Adult // C for Child // B for Baby

// 'nicotine': 'fumeur',
// 'cafeine': 'cafe',
// 'goudron': 'fumeur',
// 'gluten': 'sucreries',
// 'bisphenol': 'conserve',
// 'arsenic': 'minéral',
// 'desoxynivalenol': 'blé',
// 'plomp': 'peinture',
// 'cocaine': 'drogue',
// 'cannabis': 'drogue',
// 'gliclazide': 'diabete type 2'
// 'anabolisants': 'dopage'


const initialState = fromJS({
  selectedPatient: null,
  patients: [
  	{
      id: 1,
      name: 'Benahmed Mohammed',
      sexe: 'Homme',
      age: 'Adulte',
      signatureChimique: {
        nicotine: 'fumeur',
        cafeine: 'buveur de café',
        minoxidil: 'traitement de repoussement de cheveux'
      }
    },
  	{
      id: 2,
      name: 'Dahman Amina',
      sexe: 'Femme',
      age: 'Adulte',
      signatureChimique: {
        antidiabetique: 'diabète',
        cafeine: 'buveuse de café',
        antiinflamation: 'Inflammation'
      }
    },
  	{
      id: 3,
      name: 'Benamar Youcef',
      sexe: 'Homme',
      age: 'Adulte',
      signatureChimique: {
        goudron: 'fumeur',
        cannabis: '?',
        antiinflamation: 'Inflammation'
      }
    },
    {
      id: 4,
      name: 'Hamzi Soufiane',
      sexe: 'Homme',
      age: 'Adulte',
      signatureChimique: {
        cafeine: 'Buveur de café',
        antihistaminiques: 'Allérgie',
        Cholesterol: 'Une description'
      }
    },
    {
      id: 5,
      name: 'Zarki Soumia',
      sexe: 'Femme',
      age: 'Enfant',
      signatureChimique: {
        nicotine: 'Fumeuse',
        antihypertenseur: 'Hypertension'
      }
    },
    {
      id: 6,
      name: 'Malek Fatima',
      sexe: 'Femme',
      age: 'Enfant',
      signatureChimique: {
        antihistaminique: '?',
        cafeine: '?'
      }
    },
    {
      id: 7,
      name: 'Bouamer Mohammed',
      sexe: 'Homme',
      age: 'Adulte',
      signatureChimique: {
        antidepresseur: '?',
        opium: '?'
      }
    },
    {
      id: 8,
      name: 'Kerbal Samir',
      sexe: 'Homme',
      age: 'Enfant',
      signatureChimique: {
        inflamation: '?',
        allergie: '?'
      }
    },
    {
      id: 9,
      name: 'Chrif Rida',
      sexe: 'Homme',
      age: 'Adulte',
      signatureChimique: {
        cafeine: '?',
        antihypertenseur: '?'
      }
    },
    {
      id: 10,
      name: 'Belarbi Sarah',
      sexe: 'Femme',
      age: 'Adulte',
      signatureChimique: {
        antidiabethe: '?'
      }
    }
  ]
});

// Actions
const INCREMENT = 'CounterState/INCREMENT';
const SELECT_PATIENT = 'CounterState/SELECT_PATIENT';

// Action creators
export function increment() {
  return {type: INCREMENT};
}

export function selectPatient(id) {
  return {type: SELECT_PATIENT, id}
}

// Reducer
export default function CounterStateReducer(state = initialState, action = {}) {
  switch (action.type) {
    case INCREMENT:
      return state.update('value', value => value + 1);
    case SELECT_PATIENT:
      return state.set('selectedPatient', action.id);

    default:
      return state;
  }
}
