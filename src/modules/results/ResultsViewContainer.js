import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import ResultsView from './ResultsView';
import {NavigationActions} from 'react-navigation';
import * as ResultsStateActions from '../results/ResultsState';

export default connect(
  state => ({
    data: state.getIn(['home', 'users']),
    patients: state.getIn(['results', 'patients']),
    username: state.getIn(['login', 'username']),
    selectedUser: state.getIn(['home', 'selectedUser'])
  }),
  dispatch => {
    return {
      navigate: bindActionCreators(NavigationActions.navigate, dispatch),
      resultsStateActions: bindActionCreators(ResultsStateActions, dispatch)
    };
  }
)(ResultsView);
