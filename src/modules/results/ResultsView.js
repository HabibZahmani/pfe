import React, {PropTypes, Component} from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Image,
  ScrollView,
  Dimensions,
  View
} from 'react-native';
import { Container, Content, List, ListItem, Thumbnail, Text, Body, Left, Right, Button } from 'native-base';
import {H3} from 'nachos-ui';
import Icon from 'react-native-vector-icons/MaterialIcons';

class ResultsView extends Component {
  static displayName = 'ResultsView';

  static navigationOptions =
  ({ navigation }) => ({
    tabBarKey: navigation.state,
    tabBarLabel: 'Results',
    tabBarIcon: () => (
       <Icon name='plus-one' size={24} color="red" />
    ),
    // visible: !navigation.state.params ? true : navigation.state.params.visible,
  });

  state = {
    screenHeight: Dimensions.get('window').height
  }

  onLayout = () => {
    this.setState({
      screenHeight: Dimensions.get('window').height
    });
  }

  _renderDiseases(items) {
    return (
      <List>
        <ListItem itemDivider>
          <H3>Maladies</H3>
        </ListItem>

        {items.map((item) => {

          return(
                    <ListItem key={item} avatar>
                        <Left>
                            <Icon name='healing' size={25} color='#629e62' />
                        </Left>
                        <Body>
                            <Text>Maladie</Text>
                            <Text note>{item}</Text>
                        </Body>
                        <Right>
                            <Text note>3:43 pm</Text>
                        </Right>
                    </ListItem>
                  )
        })}
      </List>
    )
  }

  _renderDiseasesChronic(items) {
    return (
      <List>
        <ListItem itemDivider>
          <H3>Maladies Chroniques</H3>
        </ListItem>

        {items.map((item) => {

          return(
                    <ListItem key={item} avatar>
                        <Left>
                            <Icon name='healing' size={25} color='#629e62' />
                        </Left>
                        <Body>
                            <Text>Maladie</Text>
                            <Text note>{item}</Text>
                        </Body>
                        <Right>
                            <Text note>3:43 pm</Text>
                        </Right>
                    </ListItem>
                  )
        })}
      </List>
    )
  }

  _renderInfo(item) {
    return (
      <List>
        <ListItem itemDivider>
          <H3>Infos</H3>
        </ListItem>

                    <ListItem key={item.get('id')} avatar>
                        <Left>
                            <Icon name='description' size={25} color='#629e62' />
                        </Left>
                        <Body>
                            <Text>{item.get('name')}</Text>
                            <Text note>{item.get('sexe')}</Text>
                        </Body>
                        <Right>
                            <Text note>{item.get('age')}</Text>
                        </Right>
                    </ListItem>
                    {item.get('signatureChimique').entrySeq().map((it, index) => {return (
                      <ListItem key={index} avatar>
                          <Left>
                              <Icon name='description' size={25} color='#629e62' />
                          </Left>
                          <Body>
                              <Text>{it[0]}</Text>
                              <Text note>{it[1]}</Text>
                          </Body>
                      </ListItem>
                    )})}
                  </List>
        )
  }

  // signatureChimique.entrySeq().forEach(e => console.log(`key: ${e[0]}, value: ${e[1]}`));

  // _renderSignature(items) {
  //   console.log('items', items.toJS());
  //   items.forEach((item, index) => {
  //     console.log('item', item);
  //     console.log('index', index);
  //     return <Text>test</Text>
  //   });
  // }

  componentDidMount() {

  }

  render() {
    const {data, username, selectedUser} = this.props;

    if( !selectedUser) {
      return (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', flexDirection: 'row', height: this.state.screenHeight - 200}}>
          <H3>Aucun résultat à afficher</H3>
        </View>
      )
    }


    return (
      <Container>
        <Content>
          {this._renderInfo(this.props.patients.get(this.props.selectedUser))}
          <Button onPress={() => this.props.navigate({routeName: 'Home'})} style={{marginTop: 40, backgroundColor: '#ff9c00'}}>
            <Body><Text style={{color: '#FFF'}}>Analyser à nouveau</Text></Body>
          </Button>
        </Content>
      </Container>
    );
  }
}

// this.props.navigate({routeName: 'Home'})

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white'
  }
});

export default ResultsView;
